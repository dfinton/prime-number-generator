<?php

namespace PrimeNumberGenerator;

class PrimeNumberGenerator {

    /**
     * The current prime being stored as state
     *
     * @var integer $currentPrime
     */
    var $currentPrime;

    /**
     * Sets the starting number to the requested value. Afterwards, it rounds it
     * to the next higher integer and then iterates until it finds a valid prime
     * number. This way the stored valie for $currentPrime is always a prime number
     * after validation.
     *
     * @access private
     * @param float $startingNumber
     */
    public function __construct($startingNumber = 2) {
        $this->currentPrime = $startingNumber;

        $this->normalize();

        if (! $this->isPrime()) {
            $this->getNextPrime();
        }
    }

    /**
     * Makes certain that $currentPrime is always an integer
     *
     * @access private
     */
    private function normalize() {
        $this->currentPrime = (int) ceil($this->currentPrime);
    }

    /**
     * Increments $currentPrime to the next potentially valid prime number. If it's
     * currecntly set to less than 2, then it will set it to 2. If it's an even 
     * number, then it will be incremented to the next odd number. Otherwise, the value
     * is raised by 2 to the next odd number.
     *
     * @access private
     */
    private function increment() {
        if (2 > $this->currentPrime) {
            $this->currentPrime = 2;
        } elseif (0 === $this->currentPrime % 2) {
            $this->currentPrime++;
        } else {
            $this->currentPrime += 2;
        }
    }

    /**
     * The meat of the class: this method goes through all potential divisors up to
     * the square root of $currentPrime (rounded up) to see if the number is divisible
     * by any of them. If so, the method retuns false. Otherwise, it returns true. This
     * method also tests for the special cases for "2" (which is always prime), less than
     * "2" (which is never prime), or divisible by 2 (never prime)
     *
     * @access private
     * @return boolean
     */
    private function isPrime() {
        if (2 > $this->currentPrime) {
            return false;
        } else if (2 === $this->currentPrime) {
            return true;
        } else if (0 === $this->currentPrime % 2) {
            return false;
        }

        $maxDivisor = (int) ceil(sqrt($this->currentPrime));

        for ($divisor = 3; $divisor <= $maxDivisor; $divisor += 2) {
            if (0 === $this->currentPrime % $divisor) {
                return false;
            }
        }

        return true;
    }

    /**
     * Public method for iterating to the next prime number and returning its value
     *
     * @access public
     * @return integer
     */
    public function getNextPrime() {
        $this->increment();

        while (! $this->isPrime()) {
            $this->increment();
        }

        return $this->currentPrime;
    }

    /**
     * Public method for returning the currently-stored prime number
     *
     * @access public
     * @return integer
     */
    public function getCurrentPrime() {
        return $this->currentPrime;
    }
}
