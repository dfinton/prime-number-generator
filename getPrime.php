<?php

require "lib/PrimeNumberGenerator.php";

use PrimeNumberGenerator\PrimeNumberGenerator;

function printJSON($payload) {
    header('Content-Type: application/json');
    echo json_encode($payload);
}

$payload = array(
    'status' => 'ok',
    'message' => '',
    'primes' => array(),
);

$startingValue = 2;
$maxIteration = 20;

if (isset($_GET['s'])) {
    $startingValue = $_GET['s'];
}

if (isset($_GET['n'])) {
    $maxIteration = $_GET['n'];
}

if (! (is_numeric($startingValue) && is_numeric($maxIteration))) {
    $payload['status'] = 'error';
    $payload['message'] = 'Both parameters need to be a numeric value';
    printJSON($payload);
    exit;
}

$primeNumberGenerator = new PrimeNumberGenerator($startingValue);

for ($i = 0; $i < $maxIteration; $i++) {
    $primeNumber = $primeNumberGenerator->getCurrentPrime();
    $payload['primes'][] = $primeNumber;
    $primeNumberGenerator->getNextPrime();
}

printJSON($payload);
exit;
