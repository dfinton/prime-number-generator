<!DOCTYPE html>
<html>
    <head>
        <title>Prime Number Generator</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/app.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4">
                    <form id="get-primes-form" method="get" action="getPrime.php">
                        <div class="form-group">
                            <label for="starting-value">Starting Value</label>
                            <input id="starting-value" class="form-control" type="text" name="starting-value" value="2" />
                        </div>
                        <div class="form-group">
                            <label for="num-iterations">Number of Primes:</label>
                            <input id="num-iterations" class="form-control" type="text" name="num-iterations" value="20" />
                        </div>
                        <div class="form-group">
                            <input id="get-primes" class="form-control" type="submit" value="Get Primes" />
                        </div>
                    </form>
                </div>
                <div class="col-lg-4"></div>
            </div>

            <div class="row">
                <div class="col-lg-4"></div>
                <div id="prime-numbers" class="col-lg-4 prime-numbers"></div>
                <div class="col-lg-4"></div>
            </div>
        </div>

        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/app.js"></script>
    </body>
</html>
