var primeNumberGenerator = {
    init : function() {
        $('#get-primes-form').on('submit', primeNumberGenerator.getPrimes);
    },

    getPrimes : function(event) {
        event.preventDefault();

        var primesDiv = $('#prime-numbers');
        var loadImage = $('<img>').attr('src', 'images/ajax-loader.gif');

        primesDiv.html(loadImage);

        var urlPath = $(this).attr('action');
        var startingValue = $('#starting-value').val();
        var numIterations = $('#num-iterations').val();
        
        var ajaxData = {
            s : startingValue,
            n : numIterations
        };

        $.ajax({
            type : 'GET',
            url : urlPath,
            data : ajaxData,
            context: this
        }).done(primeNumberGenerator.listPrimes);
    },

    listPrimes : function (data) {
        var primesDiv = $('#prime-numbers');

        primesDiv.empty();

        if ('ok' === data.status) {
            data.primes.forEach(function(prime) {
                primesDiv.append(prime);
                primesDiv.append($('<br>'));
            });
        } else {
            primesDiv.append($('<span>').addClass('error').html('Error: ' + data.message));
        }
    }
};

$(document).ready(function() {
    primeNumberGenerator.init();
});
