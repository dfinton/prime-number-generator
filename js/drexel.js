var convertDateToString = function(dateString) {
    date = new Date(dateString);

    var months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];

    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var calendarDate = date.getDate();
    var hourMil = date.getHours();
    var hour = hourMil % 12;
    var AMorPM = hourMil < 12 ? 'AM' : 'PM';
    var minute = date.getMinutes();
    var second = date.getSeconds();

    hour = 0 === hour ? 12 : hour;

    var dateString = month + ' ' + calendarDate + ', ' + year + ' at ' + hour + ':' + minute + ':' + second + ' ' + AMorPM;

    return dateString;
};

$(document).ready(function() {
    var server = {
        ajaxURL : '/rest/daemon',

        serverStatusDisplayElem : $('div.drexel.server-status'),
        serverStatusToggleButton : $('button.drexel.server-status-toggle'),

        init : function() {
            if ($('div.drexel.primary').length) {
                this.resetDisplay();
                this.getServerStatus();

                this.serverStatusToggleButton.on('click', function(event) {
                    event.preventDefault();
                    server.toggleServerStatus($(event.target));

                    return false;
                });
            }
        },

        resetDisplay : function() {
            this.serverStatusDisplayElem.html('Obtaining server status...');
            this.serverStatusToggleButton.attr('data-server-status', 'unknown');
            this.serverStatusToggleButton.attr('disabled', 'disabled');
        },

        displayServerStatus : function(data) {
            if (data.isRunning) {
                this.serverStatusDisplayElem.html('The server is running and was started on ' + convertDateToString(data.startTime));
                this.serverStatusToggleButton.attr('data-server-status', 'up');
                this.serverStatusToggleButton.removeAttr('disabled');
            } else {
                this.serverStatusDisplayElem.html('The server is not running');
                this.serverStatusToggleButton.attr('data-server-status', 'down');
                this.serverStatusToggleButton.removeAttr('disabled');
            }
        },

        toggleServerStatus : function(target) {
            var serverStatusAttr = target.attr('data-server-status');
            var postData = {};

            if ('up' === serverStatusAttr) {
                postData.isRunning = false;
            } else if ('down' === serverStatusAttr) {
                postData.isRunning = true;
            }

            if ('unknown' !== serverStatusAttr) {
                this.resetDisplay();

                $.post(this.ajaxURL, postData).done(function(data) {
                    server.displayServerStatus(data);
                });
            }
        },

        getServerStatus : function() {
            $.get(this.ajaxURL, function(data, status) {
                server.displayServerStatus(data);
            });
        }
    };

    server.init();

    var keepAlive = {
        ajaxURL : '/rest/keepalive',
        intervalMinutes : 5,
        timerID : null,

        init : function() {
            setInterval(function() {

            }, this.intervalMinutes * 60000);
        }
    };

    keepAlive.init();
});
